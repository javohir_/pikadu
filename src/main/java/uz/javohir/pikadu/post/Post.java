package uz.javohir.pikadu.post;

import lombok.*;
import uz.javohir.pikadu.user.User;

import javax.persistence.*;


@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "posts")
public class Post {
    @Transient
    static final String sequenceName = "post_seq_name";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    private Long id;

    @Column(name = "highlight")
    private String highlight;
    @Column(name = "postText")
    private String postText;
    @Column(name = "likes")
    private Long likes;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "userId")
    private User user;

    @Column
    private Long userId;


    public Post(String highlight, String postText) {
        this.highlight = highlight;
        this.postText = postText;
    }

    public Post(String highlight, String postText,  User user) {
        this.highlight = highlight;
        this.postText = postText;
        this.user = user;
    }
}


