package uz.javohir.pikadu.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import uz.javohir.pikadu.common.Message;
import uz.javohir.pikadu.dto.RequestPostDto;
import uz.javohir.pikadu.dto.RequestUserDto;
import uz.javohir.pikadu.dto.ResponsPostDto;
import uz.javohir.pikadu.post.Post;
import uz.javohir.pikadu.repo.PostRepo;
import uz.javohir.pikadu.user.User;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class PostService {

    @Autowired
    private PostRepo postRepo;
    @Autowired
    private UserService userService;

    /*Add new post*/

    public ResponseEntity<String> addNewPost(RequestPostDto requestPostDto) {
        Optional<User> userOptional = userService.getById(requestPostDto.getUserId());
        User user = userOptional.get();
        postRepo.save(RequestPostDto.fromRequestPostDto(requestPostDto, user));
        return Message.newPost();
    }

    /*find all posts*/

    public ResponseEntity<List<ResponsPostDto>> getAllPosts() {
        List<Post> postList = postRepo.findAll();
        return ResponseEntity
                .ok(postList
                        .stream()
                        .map(ResponsPostDto :: toResponsPostDto)
                        .collect(Collectors
                                .toList()));
    }



}
