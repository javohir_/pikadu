package uz.javohir.pikadu.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import uz.javohir.pikadu.common.Message;
import uz.javohir.pikadu.dto.RequestUserDto;
import uz.javohir.pikadu.dto.ResponsUserDto;
import uz.javohir.pikadu.repo.PostRepo;
import uz.javohir.pikadu.repo.UserRepo;
import uz.javohir.pikadu.user.User;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PostRepo postRepo;


    /* Add new user */

    public ResponseEntity<String> addNewUser(@RequestBody RequestUserDto requestUserDto) {

        Optional<User> userOptional = userRepo.findUserByEmail(requestUserDto.getEmail());
        if (userOptional.isPresent()){
            return Message.emailBusy();
        }else{

            userRepo.save(RequestUserDto.fromRequestUserDto(requestUserDto));
            return Message.newUser();
        }
    }


    /* Find all users */
    public ResponseEntity<List<ResponsUserDto>> getAllUsers() {
        List<User> userList = userRepo.findAll();
        return ResponseEntity
                .ok(userList
                        .stream()
                        .map((User user) -> ResponsUserDto.toResponsUserDto(user))
                        .collect(Collectors
                                .toList()));
    }

     /* Find user by id */

    public Optional<User>  getById(Long id){
        return userRepo.findById(id);
    }

    public Object getUserById(Long id) {
        Optional<User> userOptional = userRepo.findById(id);
        if (userOptional.isPresent()) {
            return ResponseEntity
                    .ok(userOptional
                            .stream()
                            .map(ResponsUserDto :: toResponsUserDto)
                            .collect(Collectors
                                    .toList()));
        }
        return  Message.notFound();
    }


    /* Delete user by Id */

    public ResponseEntity<String> deleteUserById(Long id){
        Optional<User> userOptional = userRepo.findById(id);
        if (userOptional.isPresent()){
            userRepo.deleteById(id);
        }else {
            return Message.notFound();
        }
        return Message.deleteUser();
    }

            /* Update user */

    public ResponseEntity<String> updateUser(Long id, RequestUserDto requestUserDto) {

        Optional<User> userOptional = userRepo.findById(id);

        if (userOptional.isPresent()) {

            User user =  userOptional.get();

            user.setUsername(requestUserDto.getUsername());
            user.setPassword(requestUserDto.getPassword());
            user.setEmail(requestUserDto.getEmail());
            user.setUserPhotoUrl(requestUserDto.getUserPhotoUrl());

            userRepo.save(user);
        }else{
            return Message.notFound();
        }

        return Message.dataEdit();
    }

            /* Delete all users */

    public ResponseEntity<String> deleteAllUsers() {
        userRepo.deleteAll();
        return Message.deleteUser();
    }

    public ResponseEntity<?> update(RequestUserDto requestUserDto) {
        Optional<User> optionalUser = userRepo.findById(requestUserDto.getId());
        User user = optionalUser.get();
        if (!optionalUser.isEmpty()){
            user.setUsername(requestUserDto.getUsername());
        }
        return ResponseEntity.ok("");
    }
}
