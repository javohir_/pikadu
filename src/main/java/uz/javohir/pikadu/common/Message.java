package uz.javohir.pikadu.common;

import org.springframework.http.ResponseEntity;

public class Message {

    static String newUser = "Yangi User qo'shildi.";
    static String dataEdit = "Ma'lumotlar o'zgartirildi.";
    static String notFound = "Bunday foydalanuvchi yuq.";
    static String deleteUser = "User o'chirildi";
    static String emailBusy = "Bu email band. Boshqa email kiriting!";

    static String newPost = "Yangi post qo'shildi.";
    static String postEdit = "Post yangilandi.";
    static String postNotFound = "Bunday post topilmadi";
    static String deletePost = "Post o'chirildi";

    public static ResponseEntity<String> newUser() {
        return ResponseEntity.ok(newUser);
    }

    public static ResponseEntity<String> dataEdit(){
        return ResponseEntity.ok(dataEdit);
    }
    public static ResponseEntity<String> notFound(){
        return ResponseEntity.ok(notFound);
    }
    public static ResponseEntity<String> deleteUser(){
        return ResponseEntity.ok(deleteUser);
    }
    public static ResponseEntity<String> emailBusy(){
        return ResponseEntity.ok(emailBusy);
    }

    public static ResponseEntity<String> newPost(){
        return  ResponseEntity.ok(newPost);
    }
    public static ResponseEntity<String> postEdit(){
        return ResponseEntity.ok(postEdit);
    }
    public static ResponseEntity<String> postNotFound(){
        return ResponseEntity.ok(postNotFound);
    }
    public static ResponseEntity<String> deletePost(){
        return ResponseEntity.ok(deletePost);
    }
}
