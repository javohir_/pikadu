package uz.javohir.pikadu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import uz.javohir.pikadu.post.Post;
import uz.javohir.pikadu.user.User;

@JsonInclude(JsonInclude.Include.USE_DEFAULTS)
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class RequestPostDto {

    private String highlight;
    private String postText;
    private Long userId;

     public static Post fromRequestPostDto(RequestPostDto requestPostDto, User user){
        return new Post(requestPostDto.getHighlight(), requestPostDto.getPostText(), user);
    }

    public Long getUserId() {
         return this.userId;
    }
}
