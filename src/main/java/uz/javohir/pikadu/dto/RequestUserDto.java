package uz.javohir.pikadu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import uz.javohir.pikadu.user.User;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
//@NoArgsConstructor
@RequiredArgsConstructor
public class RequestUserDto {

    private Long id;
    private String username;
    private String password;
    private String email;
    private String userPhotoUrl;

    public static User fromRequestUserDto(@RequestBody RequestUserDto requestUserDto){
        return new User(requestUserDto.getUsername(), requestUserDto.getPassword(),
                requestUserDto.getEmail(), requestUserDto.getUserPhotoUrl());
    }

}
