package uz.javohir.pikadu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import uz.javohir.pikadu.post.Post;
import uz.javohir.pikadu.user.User;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
//@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class ResponsPostDto {

    private String highlight;
    private String postText;

    public static ResponsPostDto toResponsPostDto(Post post){
        return new ResponsPostDto(post.getHighlight(), post.getPostText());
    }

}
