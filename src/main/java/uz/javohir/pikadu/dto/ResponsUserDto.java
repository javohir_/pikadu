package uz.javohir.pikadu.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import uz.javohir.pikadu.post.Post;
import uz.javohir.pikadu.user.User;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class ResponsUserDto {

    private String username;
    private String userPhotoUrl;
    private List<Post> postList;

    public ResponsUserDto(String username, String userPhotoUrl, String highlight, String postText) {

    }

    public static ResponsUserDto toResponsUserDto(User user){
        return new ResponsUserDto(user.getUsername(), user.getUserPhotoUrl());
    }

    public ResponsUserDto(String username, String userPhotoUrl) {
        this.username = username;
        this.userPhotoUrl = userPhotoUrl;
    }
}
