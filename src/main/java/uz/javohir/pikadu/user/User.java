package uz.javohir.pikadu.user;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.javohir.pikadu.post.Post;
//import uz.javohir.pikadu.post.Post;

import javax.persistence.*;
import java.util.List;
//import java.util.List;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Transient
    static final String sequenceName = "user_seq_name";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SequenceGenerator(name = sequenceName, sequenceName = sequenceName, allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "passwod")
    private String password;

    @NotNull
    @Column(name = "email")
    private String email;

    @Column(name = "userPhotoUrl")
    private String userPhotoUrl;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "userId")
    private List<Post> postList;

    public User(String username, String password, String email, String userPhotoUrl ) {
            this.username = username;
            this.password = password;
            this.email = email;
            this.userPhotoUrl = userPhotoUrl;
    }



//    public User(String requestUserDtoUsername, String username, String password, String email) {
//        this.username = username;
//        this.password = password;
//        this.email = email;
//    }

}
