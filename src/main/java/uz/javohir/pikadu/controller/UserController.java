package uz.javohir.pikadu.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.javohir.pikadu.dto.RequestUserDto;
import uz.javohir.pikadu.dto.ResponsUserDto;
import uz.javohir.pikadu.service.UserService;

import java.util.List;

@RestController
//@Controller
@Data
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/addNewUser")
    public ResponseEntity<String> addNewUser(@RequestBody RequestUserDto requestUserDto){
        return userService.addNewUser(requestUserDto);
    }

    @GetMapping("/getAllUsers")
    public ResponseEntity<List<ResponsUserDto>> getAllUsers(){
        return userService.getAllUsers();
    }
    @GetMapping("/getUserById/{id}")
    public Object getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }

    @PutMapping("/updateUser/{id}")
    public ResponseEntity<String> updateUser(@PathVariable(value = "id") Long id,
                                          @RequestBody RequestUserDto requestUserDto){
        return userService.updateUser(id, requestUserDto);
    }

    @DeleteMapping("/deleteAllUsers")
    public ResponseEntity<String> deleteAllUsers(){
       return userService.deleteAllUsers();
    }

    @DeleteMapping(value = "/deleteUserById/{id}")
    public ResponseEntity<String> deleteById(@PathVariable(value = "id") Long id){
        return userService.deleteUserById(id);

    }

    @PutMapping("/update")
    public ResponseEntity<?> updateUser(@RequestBody RequestUserDto requestUserDto){
        return userService.update(requestUserDto);
    }
}
