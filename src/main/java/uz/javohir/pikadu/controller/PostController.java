package uz.javohir.pikadu.controller;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.javohir.pikadu.common.Message;
import uz.javohir.pikadu.dto.RequestPostDto;
import uz.javohir.pikadu.dto.ResponsPostDto;
import uz.javohir.pikadu.service.PostService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@Data
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping("/getAllPosts")
    public ResponseEntity<List<ResponsPostDto>> getAllPosts() {
        return postService.getAllPosts();
    }

    @PostMapping("/addNewPost")
    public ResponseEntity<String> addNewPost(@RequestBody RequestPostDto requestPostDto) {
        return postService.addNewPost(requestPostDto);
    }
}
