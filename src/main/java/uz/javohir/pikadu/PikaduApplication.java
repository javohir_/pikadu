package uz.javohir.pikadu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PikaduApplication {

    public static void main(String[] args) {
        SpringApplication.run(PikaduApplication.class, args);
    }

}
