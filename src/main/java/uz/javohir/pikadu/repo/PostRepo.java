package uz.javohir.pikadu.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.javohir.pikadu.post.Post;
import uz.javohir.pikadu.user.User;

import java.util.List;

@Repository
public interface PostRepo extends JpaRepository<Post, Long> {
    public List<Post> findPostByUserId(Long id);

}
